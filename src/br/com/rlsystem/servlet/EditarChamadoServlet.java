package br.com.rlsystem.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class EditarChamadoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		PrintWriter out = response.getWriter();

		try {
			try {
				Class.forName("com.mysql.jdbc.Driver");

				Connection conn;
				conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/chamados", "root", "");

				String sql = " select * from chamado where id = ?";

				Integer id = Integer.parseInt(request.getParameter("id"));

				PreparedStatement stm = conn.prepareStatement(sql);
				stm.setInt(1, id);
				ResultSet rs = stm.executeQuery();

				if (rs.next()) {

					out.println("<html>");
					out.println("<head>");
					out.println("<title>Editar chamado</title>");
					out.println("</head>");
					out.println("<body>");
					out.println("<h1>Preencha as informa��es do chamado</h1>");
					out.println("<hr/>");
					out.println("<form method='POST'>");
					out.println("ID:<br> <input type='text' name='id' readonly='readonly' value='" + rs.getInt("id")
							+ "'>");

					out.println(
							"T�tulo:<br> <input type='text' name='txtTitulo' value='" + rs.getString("titulo") + "'>");
					out.println("<br>");
					out.println("Conte�do:<br> <textarea name='txtConteudo' rows='10' cols='10'>"
							+ rs.getString("conteudo") + "</textarea> ");
					out.println("<br>");
					out.println("<input type='submit' value='Editar chamado'>");
					out.println("<form/>");
					out.println("<br>");
					out.println("<a href='http://localhost:8081/chamado/novochamado'>Novo chamado</a>");
					out.println("<a href='http://localhost:8081/chamado/listachamado'>Lista Chamado</a>");
					out.println("<a href='http://localhost:8081/chamado/sair'>Sair</a>");
					out.println("</body>");
					out.println("</html>");
				}

				stm.close();
				conn.close();
				
				//response.sendRedirect("http://localhost:8081/chamado/listachamado");


			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		
	}
	
protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out =  response.getWriter();
		int id  = Integer.parseInt( request.getParameter("id"));
		String titulo = request.getParameter("txtTitulo");
		String conteudo = request.getParameter("txtConteudo");
		
		if(titulo.trim().length() < 4) {
			
		}else if(conteudo.trim().length() < 4 ) {
			
		}else {
			
			try {
				Class.forName("com.mysql.jdbc.Driver");
				
				String sql  = " update chamado set titulo = ?, conteudo = ? where id = ?";
							
				try {
					Connection  conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/chamados", "root", "");
					PreparedStatement pstm  = conn.prepareStatement(sql);
					pstm.setString(1, titulo);
					pstm.setString(2, conteudo);
					pstm.setInt(3, id);
					pstm.execute();
					pstm.close();
					conn.close();
					
					response.sendRedirect("http://localhost:8081/chamado/listachamado");
					
				} catch (SQLException e) {
	
					out.println("Problema com banco de dados" + e.getMessage());
				}
				
				
			} catch (ClassNotFoundException e) {
				out.println("Problema ao carregar o drive de conex�o!");
			}
		}

	}

}
