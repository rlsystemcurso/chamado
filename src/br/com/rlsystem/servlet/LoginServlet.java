package br.com.rlsystem.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		try {
			PrintWriter out =  response.getWriter();
			
			String login_name = "";
			Cookie[] cookie = request.getCookies();
			
			if(cookie != null) {
				for(Cookie ck : cookie) {
					
					if(ck.getName().equals("login_name")) {
						login_name = ck.getValue();
					}
				}
			}
			
			out.println("<html>");
			out.println("<head>");
			out.println("<title>Login</title>");
			out.println("</head>");
			out.println("<body>");
			out.println("<h1>Preencha as informações para logar</h1>");
			out.println("<hr/>");
			out.println("<form method='POST'>");
			
			if( request.getParameter("msg") != null) {
				if( request.getParameter("msg").equals("logoff")) {
					HttpSession session = request.getSession();
					
					session.invalidate();
					out.print("Saindo...");
				}
			}
			
			if( request.getParameter("msg") != null) {
				if( request.getParameter("msg").equals("error")) {
					out.println("<span>dados inválidos</span><br><br>");
				}
			}
			out.println("Login:<br> <input type='text' name='login' value='"+ login_name +"'>");
			out.println("<br>");
			out.println("Senha:<br> <input type='password' name='senha'>");
			out.println("<br>");
			out.println("<br>");
			out.println("<input type='submit' value='Logar'>");
			out.println("<form/>");
			out.println("</body>");
			out.println("</html>");
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
			String login = request.getParameter("login");
			String senha = request.getParameter("senha");
			
			Cookie ck = new Cookie("login_name", login);
			ck.setMaxAge(60*60*24*30*12);
			
			response.addCookie(ck);
			
			try {
				Class.forName("com.mysql.jdbc.Driver");
				
				try {
					Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/chamados", "root", "");
					
					String sql = "select * from usuarios where login = ? and senha = ?";
					
					PreparedStatement pstm = conn.prepareStatement(sql);
					pstm.setString(1, login);
					pstm.setString(2,senha);
					
					ResultSet rs  =  pstm.executeQuery();
				
					if(rs.next()) {
						
						HttpSession session = request.getSession();
						
						session.setAttribute("login", login);
						
						response.sendRedirect("http://localhost:8081/chamado/listachamado");						
					}else {
						pstm.close();
						conn.close();
						response.sendRedirect("http://localhost:8081/chamado/login?msg=error");						

					}
					
				} catch (SQLException e) {
					
					e.printStackTrace();
				}
				
				
			} catch (ClassNotFoundException e) {
				
				e.printStackTrace();
			}
			
			
			
	}

}
