package br.com.rlsystem.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class IndexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			PrintWriter out =  response.getWriter();
			
			out.println("<html>");
			out.println("<head>");
			out.println("<title>Curso Servlet e JSP RL System</title>");
			out.println("</head>");
			out.println("<body>");
			out.println("<h1>Sistema de Chamados Curso Servlet e JSP RL System</h1>");
			out.println("<hr>");
			out.println("<a href='http://localhost:8081/chamado/novochamado'>Novo chamado</a>");
			out.println("<a href='http://localhost:8081/chamado/listachamado'>Lista Chamado</a>");
			out.println("<a href='http://localhost:8081/chamado/sair'>Sair</a>");
			out.println("</body>");
			out.println("</html>");
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
