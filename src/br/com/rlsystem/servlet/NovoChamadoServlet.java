package br.com.rlsystem.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class NovoChamadoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		try {
			PrintWriter out =  response.getWriter();
			
			out.println("<html>");
			out.println("<head>");
			out.println("<title>Novo chamado</title>");
			out.println("</head>");
			out.println("<body>");
			out.println("<h1>Preencha as informa��es do chamado</h1>");
			out.println("<hr/>");
			out.println("<form method='POST'>");
			out.println("T�tulo:<br> <input type='text' name='txtTitulo'>");
			out.println("<br>");
			out.println("Conte�do:<br> <textarea name='txtConteudo' rows='10' cols='10'></textarea> ");
			out.println("<br>");
			out.println("<input type='submit' value='Abrir chamado'>");
			out.println("<form/>");
			out.println("<br>");
			out.println("<a href='http://localhost:8081/chamado/novochamado'>Novo chamado</a>");
			out.println("<a href='http://localhost:8081/chamado/listachamado'>Lista Chamado</a>");
			out.println("<a href='http://localhost:8081/chamado/sair'>Sair</a>");
			out.println("</body>");
			out.println("</html>");
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out =  response.getWriter();
		String titulo = request.getParameter("txtTitulo");
		String conteudo = request.getParameter("txtConteudo");
		
		if(titulo.trim().length() < 4) {
			
		}else if(conteudo.trim().length() < 4 ) {
			
		}else {
			
			try {
				Class.forName("com.mysql.jdbc.Driver");
				
				String sql  = " insert into chamado (titulo, conteudo) values(?, ?)";
							
				try {
					Connection  conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/chamados", "root", "");
					PreparedStatement pstm  = conn.prepareStatement(sql);
					pstm.setString(1, titulo);
					pstm.setString(2, conteudo);
					pstm.execute();
					pstm.close();
					conn.close();
					
					response.sendRedirect("http://localhost:8081/chamado/listachamado");
					
				} catch (SQLException e) {
	
					out.println("Problema com banco de dados" + e.getMessage());
				}
				
				
			} catch (ClassNotFoundException e) {
				out.println("Problema ao carregar o drive de conex�o!");
			}
		}

	}

}
