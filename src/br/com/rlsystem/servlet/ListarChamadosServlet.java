package br.com.rlsystem.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ListarChamadosServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		PrintWriter out = response.getWriter();

		HttpSession session = request.getSession();

		if (session.getAttribute("login") == null) {
			response.sendRedirect("http://localhost:8081/chamado/login");
		}
		
		out.println("<br><a href='http://localhost:8081/chamado/login?msg=logoff'>Sair</a><br><br> ");

		try {
			Class.forName("com.mysql.jdbc.Driver");

			try {

				Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/chamados", "root", "");

				if (request.getParameter("id") != null) {

					int id = Integer.parseInt(request.getParameter("id"));
					String deletesql = " delete from chamado where id = ?";

					PreparedStatement pstmdelete = conn.prepareStatement(deletesql);
					pstmdelete.setInt(1, id);
					pstmdelete.execute();
				}

				String sql = " select * from chamado";
				PreparedStatement pstm = conn.prepareStatement(sql);

				ResultSet rs = pstm.executeQuery(sql);

				out.println("<table width='100%'>");

				out.println("<tr>");
				out.println("<td>ID</td>");
				out.println("<td>Titulo</td>");
				out.println("<td>Conteudo</td>");
				out.println("<td>Editar</td>");
				out.println("<td>Apagar</td>");
				out.println("</tr>");

				while (rs.next()) {

					out.println("<td>" + rs.getInt("id") + "</td>");
					out.println("<td>" + rs.getString("titulo") + "</td>");
					out.println("<td>" + rs.getString("conteudo") + "</td>");
					out.println("<td><a href='http://localhost:8081/chamado/editarchamado?id=" + rs.getInt("id")
							+ "'>[EDITAR]</a></td>");
					out.println("<td><a href='http://localhost:8081/chamado/listachamado?id=" + rs.getInt("id")
							+ "'>[APAGAR]</a></td>");
					out.println("</tr>");
				}

				out.println("</table>");

				pstm.close();
				conn.close();

			} catch (SQLException e) {

				out.println("Problema com banco de dados" + e.getMessage());
			}

		} catch (ClassNotFoundException e) {
			out.println("Problema ao carregar o drive de conex�o!");
		}
	}

}
